cmake_minimum_required(VERSION 3.12)
project(opengl_course_project)

set(CMAKE_CXX_STANDARD 11)

add_executable(opengl_course_project glad.c main.cpp utility/Shader.cpp utility/Shader.h include/stb_image.cpp include/stb_image.h Camera.cpp Camera.h shapes/Cube.cpp shapes/Cube.h Scene.cpp Scene.h utility/ResourceManager.cpp utility/ResourceManager.h shapes/Icosahedron.cpp shapes/Icosahedron.h shapes/Shape.cpp shapes/Shape.h utility/ShapeRenderer.cpp utility/ShapeRenderer.h shapes/HyperRectangle.cpp shapes/HyperRectangle.h shapes/collisions/BoundBox.cpp shapes/collisions/BoundBox.h)

find_package(OpenGL REQUIRED)
include_directories(${OpenGL_INCLUDE_DIRS})
target_link_libraries(opengl_course_project ${OpenGL_LIBRARIES})

set(SOURCE_FILES glad.c main.cpp)
target_link_libraries(opengl_course_project GL GLU glfw3 X11 Xxf86vm Xrandr pthread Xi dl Xinerama Xcursor assimp)