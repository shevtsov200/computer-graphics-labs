//
// Created by root on 12.11.18.
//

#include "Camera.h"

Camera::Camera(glm::vec3 position, glm::vec3 worldUp, float yaw, float pitch)
        : front_(glm::vec3(0.0f, 0.0f, -1.0f)),
          movementSpeed_(SPEED_DEFAULT),
          mouseSensitivity_(SENSITIVITY_DEFAULT),
          zoom_(ZOOM_DEFAULT) {
    position_ = position;
    worldUp_ = worldUp;
    yaw_ = yaw;
    pitch_ = pitch;

    updateCameraVectors();
}

Camera::Camera(float posX, float posY, float posZ, float upX, float upY,
               float upZ, float yaw, float pitch)
        : front_(glm::vec3(0.0f, 0.0f, -1.0f)),
          movementSpeed_(SPEED_DEFAULT),
          mouseSensitivity_(
                  SENSITIVITY_DEFAULT),
          zoom_(ZOOM_DEFAULT) {
    position_ = glm::vec3(posX, posY, posZ);
    worldUp_ = glm::vec3(upX, upY, upZ);
    yaw_ = yaw;
    pitch_ = pitch;

    updateCameraVectors();
}

glm::mat4 Camera::getViewMatrix() {
    return glm::lookAt(position_, position_ + front_, up_);
}


void Camera::processKeyboard(CameraMovement direction, float deltaTime) {
    float velocity = movementSpeed_ * deltaTime;

    switch (direction) {
        case FORWARD:
            position_ += front_ * velocity;
            break;
        case BACKWARD:
            position_ -= front_ * velocity;
            break;
        case LEFT:
            position_ -= right_ * velocity;
            break;
        case RIGHT:
            position_ += right_ * velocity;
            break;
    }
}

void Camera::processMouseMovement(float xOffset, float yOffset,
                                  GLboolean isPitchConstrained) {
    xOffset *= mouseSensitivity_;
    yOffset *= mouseSensitivity_;

    yaw_ += xOffset;
    pitch_ += yOffset;

    const float PITCH_CONSTRAINT = 89.0f;
    if (isPitchConstrained) {
        if (pitch_ > PITCH_CONSTRAINT) {
            pitch_ = PITCH_CONSTRAINT;
        } else if (pitch_ < -PITCH_CONSTRAINT) {
            pitch_ = -PITCH_CONSTRAINT;
        }
    }

    updateCameraVectors();
}

void Camera::processMouseScroll(float yOffset) {
    const float MIN_ZOOM = 1.0f;
    const float MAX_ZOOM = 45.0f;

    if (zoom_ >= MIN_ZOOM && zoom_ <= MAX_ZOOM) {
        zoom_ -= yOffset;
    } else if (zoom_ <= MIN_ZOOM) {
        zoom_ = MIN_ZOOM;
    } else if (zoom_ >= MAX_ZOOM) {
        zoom_ = MAX_ZOOM;
    }
}

float Camera::getZoom() const {
    return zoom_;
}

void Camera::updateCameraVectors() {
    glm::vec3 front;

    front.x = cos(glm::radians(yaw_)) * cos(glm::radians(pitch_));
    front.y = sin(glm::radians(pitch_));
    front.z = sin(glm::radians(yaw_)) * cos(glm::radians(pitch_));
    front_ = glm::normalize(front);

    right_ = glm::normalize(glm::cross(front_, worldUp_));
    up_ = glm::normalize(glm::cross(right_, front_));
}

glm::vec3 Camera::getPosition() const {
    return position_;
}

