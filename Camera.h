//
// Created by root on 12.11.18.
//

#ifndef OPENGL_COURSE_PROJECT_CAMERA_H
#define OPENGL_COURSE_PROJECT_CAMERA_H

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glad/glad.h>

enum CameraMovement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

const float YAW_DEFAULT = -90.0f;
const float PITCH_DEFAULT = 0.0f;
const float SPEED_DEFAULT = 2.5f;
const float SENSITIVITY_DEFAULT = 0.1f;
const float ZOOM_DEFAULT = 45.0f;

class Camera {
public:
    explicit Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
                    glm::vec3 worldUp = glm::vec3(0.0f, 1.0f, 0.0f),
                    float yaw = YAW_DEFAULT,
                    float pitch = PITCH_DEFAULT);

    Camera(float posX, float posY, float posZ, float upX, float upY,
           float upZ, float yaw, float pitch);

    glm::mat4 getViewMatrix();

    float getZoom() const;

    glm::vec3 getPosition() const;

    void processKeyboard(CameraMovement direction, float deltaTime);

    void processMouseMovement(float xOffset, float yOffset,
                              GLboolean isPitchConstrained = true);

    void processMouseScroll(float yOffset);

private:
    glm::vec3 position_;
    glm::vec3 front_;
    glm::vec3 up_;
    glm::vec3 right_;
    glm::vec3 worldUp_;

    float yaw_;
    float pitch_;

    float movementSpeed_;
    float mouseSensitivity_;
    float zoom_;

    void updateCameraVectors();
};


#endif //OPENGL_COURSE_PROJECT_CAMERA_H
