//
// Created by root on 20.11.18.
//

#include <glm/glm.hpp>
#include <iostream>
#include <glm/ext/matrix_clip_space.hpp>
#include <algorithm>
#include "Scene.h"
#include "utility/ResourceManager.h"

Scene::Scene(GLuint width, GLuint height) : camera_(INITIAL_CAMERA_POSITION),
                                            width_(width),
                                            height_(height) {
}

void Scene::init(GLFWwindow *window) {
    glfwSetWindowUserPointer(window, &camera_);

    ResourceManager::loadShader(
            LIGHT_SHADER_NAME,
            LIGHT_SHADER_NAME,
            nullptr,
            LIGHT_SHADER_NAME);

    ResourceManager::loadShader(
            SHAPE_SHADER_NAME,
            SHAPE_SHADER_NAME,
            SHAPE_SHADER_NAME,
            SHAPE_SHADER_NAME
    );

    ResourceManager::loadShader(
            NORMAL_SHADER_NAME,
            NORMAL_SHADER_NAME,
            NORMAL_SHADER_NAME,
            NORMAL_SHADER_NAME
    );

    shapes_.emplace(LIGHT_SOURCE_KEY, std::make_shared<Cube>(LIGHT_CUBE_SIZE));
    shapes_.emplace(ICOSAHEDRON_KEY, std::make_shared<Icosahedron>(ICOSAHEDRON_SIZE));
    shapes_.emplace(HYPER_RECTANGLE_KEY, std::make_shared<HyperRectangle>(HYPER_RECTANGLE_SIZE));

    for (const auto &shapeKeyValue : shapes_) {
        shapeRenderers_.emplace_back(*shapeKeyValue.second);
    }

    std::shared_ptr<Shape> lightCube = shapes_.at(LIGHT_SOURCE_KEY);
    std::shared_ptr<Shape> icosahedron = shapes_.at(ICOSAHEDRON_KEY);
    std::shared_ptr<Shape> hyperRectangle = shapes_.at(HYPER_RECTANGLE_KEY);

    lightCube->setPositionWorld(LIGHT_SOURCE_INITIAL_POSITION);
    icosahedron->setPositionWorld(ICOSAHEDRON_INITIAL_POSITION);
    hyperRectangle->setPositionWorld(HYPER_RECTANGLE_INITIAL_POSITION);

    lightCube->setRotationAngles(LIGHT_SOURCE_INITIAL_ROTATION);
    icosahedron->setRotationAngles(ICOSAHEDRON_INITIAL_ROTATION);
    hyperRectangle->setRotationAngles(HYPER_RECTANGLE_INITIAL_ROTATION);

}

void Scene::processInput(GLFWwindow *window, GLfloat deltaTime) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        camera_.processKeyboard(FORWARD, deltaTime);
    }

    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        camera_.processKeyboard(BACKWARD, deltaTime);
    }

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        camera_.processKeyboard(LEFT, deltaTime);
    }

    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        camera_.processKeyboard(RIGHT, deltaTime);
    }

    const float movementSpeed = 0.6;
    float velocity = movementSpeed * deltaTime;

    std::shared_ptr<Shape> lightCube = shapes_.at(LIGHT_SOURCE_KEY);
    glm::vec3 lightCubePosition = lightCube->getPositionWorld();

    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
        lightCubePosition.y += velocity;
    }

    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        lightCubePosition.y -= velocity;
    }

    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        lightCubePosition.x -= velocity;
    }

    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        lightCubePosition.x += velocity;
    }

    if (glfwGetKey(window, GLFW_KEY_8) == GLFW_PRESS) {
        lightCubePosition.z += velocity;
    }

    if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
        lightCubePosition.z -= velocity;
    }

    lightCube->setPositionWorld(lightCubePosition);

    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
        std::shared_ptr<Shape> icosahedron = shapes_.at(ICOSAHEDRON_KEY);
        icosahedron->setPositionWorld(ICOSAHEDRON_INITIAL_POSITION);
    }
}

void Scene::update(GLfloat deltaTime) {
    for (const auto &shapeKeyValue : shapes_) {
        shapeKeyValue.second->transform();
    }

    std::shared_ptr<Shape> icosahedron = shapes_.at(ICOSAHEDRON_KEY);
    std::shared_ptr<Shape> hyperRectangle = shapes_.at(HYPER_RECTANGLE_KEY);

    BoundBox icosahedronBoundBoxWorld = icosahedron->getBoundBox()
            .getBoundBoxWorldCoordinates(icosahedron->getModel(), icosahedron->getVertices());

    BoundBox planeBoundBoxWorld = hyperRectangle->getBoundBox()
            .getBoundBoxWorldCoordinates(hyperRectangle->getModel(), hyperRectangle->getVertices());


    glm::vec3 icosahedronPosition = icosahedron->getPositionWorld();

    if (!icosahedronBoundBoxWorld.intersects(planeBoundBoxWorld)) {
        icosahedronPosition.y -= icosahedronSpeed_.y * deltaTime;
        icosahedronSpeed_.y += GRAVITATIONAL_ACCELERATION;
    } else {
        icosahedronPosition.y += icosahedronBoundBoxWorld.getOverlap(planeBoundBoxWorld).y;
        icosahedronSpeed_.y = 0;
    }

    icosahedron->setPositionWorld(icosahedronPosition);
}

void Scene::render() {
    glm::mat4 projection = glm::perspective(glm::radians(camera_.getZoom()),
                                            (float) width_ / (float) height_, 0.1f, 100.0f);
    glm::mat4 view = camera_.getViewMatrix();

    Shader lightShader = ResourceManager::getShader(LIGHT_SHADER_NAME);

    ShapeRenderer lightRenderer = shapeRenderers_[0];
    ShapeRenderer icosahedronRenderer = shapeRenderers_[1];
    ShapeRenderer hyperRectangleRenderer = shapeRenderers_[2];

    std::shared_ptr<Shape> lightCube = shapes_.at(LIGHT_SOURCE_KEY);
    std::shared_ptr<Shape> icosahedron = shapes_.at(ICOSAHEDRON_KEY);
    std::shared_ptr<Shape> hyperRectangle = shapes_.at(HYPER_RECTANGLE_KEY);

    lightRenderer.drawShape(*lightCube, lightShader, projection, view);

    const glm::vec3 lightColor = glm::vec3(1.0f);

    Shader shapeShader = ResourceManager::getShader(SHAPE_SHADER_NAME);
    shapeShader.use();
    shapeShader.setVec3Uniform("objectColor", 0.2f, 0.8f, 0.2f);
    shapeShader.setVec3Uniform("lightColor", lightColor);
    shapeShader.setVec3Uniform("lightPositionWorld", lightCube->getPositionWorld());
    shapeShader.setVec3Uniform("camera_PositionWorld", camera_.getPosition());

    icosahedronRenderer.drawShape(*icosahedron, shapeShader, projection, view);

    const glm::vec3 normalColor = glm::vec3(1.0f, 1.0f, 0.0f);
    const glm::vec3 edgeColor = glm::vec3(0.0f, 1.0f, 0.0f);

    Shader normalShader = ResourceManager::getShader(NORMAL_SHADER_NAME);
    normalShader.use();
    normalShader.setVec3Uniform("lightPositionWorld", lightCube->getPositionWorld());
    normalShader.setVec3Uniform("camera_PositionWorld", camera_.getPosition());
    normalShader.setVec3Uniform("shapeCenterPosition", icosahedron->getBoundBox().getCenter());
    normalShader.setVec3Uniform("lightColor", lightColor);
    normalShader.setVec3Uniform("normalColor", normalColor);
    normalShader.setVec3Uniform("edgeColor", edgeColor);

    icosahedronRenderer.drawShape(*icosahedron, normalShader, projection, view);


    hyperRectangleRenderer.drawShape(*hyperRectangle, shapeShader, projection, view);
    normalShader.setVec3Uniform("shapeCenterPosition", hyperRectangle->getBoundBox().getCenter());
    hyperRectangleRenderer.drawShape(*hyperRectangle, normalShader, projection, view);
}

void Scene::clear() {
    ShapeRenderer lightRenderer = shapeRenderers_[0];
    ShapeRenderer icosahedronRenderer = shapeRenderers_[1];
    ShapeRenderer hyperRectangleRenderer = shapeRenderers_[2];

    lightRenderer.clear();
    icosahedronRenderer.clear();
    hyperRectangleRenderer.clear();
}
