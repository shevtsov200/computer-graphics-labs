//
// Created by root on 20.11.18.
//

#ifndef OPENGL_COURSE_PROJECT_SCENE_H
#define OPENGL_COURSE_PROJECT_SCENE_H


#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <memory>
#include <map>
#include "Camera.h"
#include "shapes/Icosahedron.h"
#include "shapes/HyperRectangle.h"
#include "utility/Shader.h"
#include "utility/ShapeRenderer.h"
#include "shapes/Cube.h"

class Scene {
public:
    Scene(GLuint width, GLuint height);

    ~Scene() = default;

    void init(GLFWwindow *window);

    void processInput(GLFWwindow *window, GLfloat deltaTime);

    void update(GLfloat deltaTime);

    void render();

    void clear();

private:
    const unsigned int LIGHT_SOURCE_KEY = 1;
    const unsigned int ICOSAHEDRON_KEY = 2;
    const unsigned int HYPER_RECTANGLE_KEY = 3;

    const glm::vec3 LIGHT_SOURCE_INITIAL_POSITION = glm::vec3(2.0f, 0.0f, 0.0f);
    const glm::vec3 ICOSAHEDRON_INITIAL_POSITION = glm::vec3(0.0f, 2.0f, 0.0f);
    const glm::vec3 HYPER_RECTANGLE_INITIAL_POSITION = glm::vec3(0.0f, 0.0f, 0.0f);

    const glm::vec3 LIGHT_SOURCE_INITIAL_ROTATION = glm::vec3(0.0f, 0.0f, 0.0f);
    const glm::vec3 ICOSAHEDRON_INITIAL_ROTATION = glm::vec3(0.0f, 0.0f, 0.0f);
    const glm::vec3 HYPER_RECTANGLE_INITIAL_ROTATION = glm::vec3(0.0f, 0.0f, 0.0f);

    const glm::vec3 INITIAL_CAMERA_POSITION = glm::vec3(0.0f, 0.0f, 3.0f);

    const float LIGHT_CUBE_SIZE = 0.01f;
    const float ICOSAHEDRON_SIZE = 0.5;
    const glm::vec3 HYPER_RECTANGLE_SIZE = glm::vec3(8.0f, 0.5f, 2.0f);

    const GLchar *SHAPE_SHADER_NAME = "cube";
    const GLchar *LIGHT_SHADER_NAME = "lamp";
    const GLchar *NORMAL_SHADER_NAME = "normal_visualization";

    glm::vec3 icosahedronSpeed_ = glm::vec3(0.0f);
    const float GRAVITATIONAL_ACCELERATION = 0.01;

    GLuint width_;
    GLuint height_;

    Camera camera_;

    std::vector<ShapeRenderer> shapeRenderers_;
    std::map<unsigned int, std::shared_ptr<Shape>> shapes_;
};


#endif //OPENGL_COURSE_PROJECT_SCENE_H
