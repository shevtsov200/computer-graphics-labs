

#include "utility/Shader.h"

#include <GLFW/glfw3.h>

#include "Camera.h"
#include "shapes/Cube.h"
#include "utility/ResourceManager.h"
#include "shapes/Icosahedron.h"
#include "Scene.h"
#include "utility/ShapeRenderer.h"

#include <glm/gtc/type_ptr.hpp>

#include <sstream>
#include <algorithm>

// callbacks
void framebuffer_size_callback(GLFWwindow *window, int width, int height);

void mouse_callback(GLFWwindow *window, double xpos, double ypos);

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);

const unsigned int WINDOW_WIDTH = 800;
const unsigned int WINDOW_HEIGHT = 600;

bool isMouseInitialised = false;
double lastX = (double) WINDOW_WIDTH / 2;
double lastY = (double) WINDOW_HEIGHT / 2;

void initializeGlfw() {
    const int TARGET_CONTEXT_VERSION = 3;

    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, TARGET_CONTEXT_VERSION);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, TARGET_CONTEXT_VERSION);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
}

GLFWwindow *createWindow() {
    const char *PROJECT_TITLE = "HELLO WINDOW";

    GLFWwindow *window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, PROJECT_TITLE, nullptr, nullptr);
    if (window == nullptr) {
        glfwTerminate();

        std::stringstream errorMessage;
        errorMessage << "Failed to create GLFW window" << std::endl;
        throw std::runtime_error(errorMessage.str());
    }

    return window;
}

void initialiseGlad() {
    auto glaDloadproc = (GLADloadproc) glfwGetProcAddress;
    if (!gladLoadGLLoader(glaDloadproc)) {
        std::stringstream errorMessage;
        errorMessage << "Failed to initialize GLAD" << std::endl;
        throw std::runtime_error(errorMessage.str());
    }
}

void run(GLFWwindow *window) {
    Scene scene(WINDOW_WIDTH, WINDOW_HEIGHT);

    scene.init(window);

    float lastTime = 0.0f;
    while (!glfwWindowShouldClose(window)) {
        float currentTime = glfwGetTime();
        float deltaTime = currentTime - lastTime;
        lastTime = currentTime;

        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        scene.processInput(window, deltaTime);

        scene.update(deltaTime);

        scene.render();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    scene.clear();

    glfwTerminate();

}

int main() {
    initializeGlfw();

    GLFWwindow *window = createWindow();
    glfwMakeContextCurrent(window);

    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    initialiseGlad();

    glEnable(GL_DEPTH_TEST);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    run(window);

    return 0;
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
    glViewport(0, 0, width, height);
}

void mouse_callback(GLFWwindow *window, double xpos, double ypos) {
    if (!isMouseInitialised) {
        lastX = xpos;
        lastY = ypos;
        isMouseInitialised = true;
    }

    float xOffset = xpos - lastX;
    float yOffset = lastY - ypos;

    lastX = xpos;
    lastY = ypos;

    auto *camera = reinterpret_cast<Camera *> (glfwGetWindowUserPointer(window));
    camera->processMouseMovement(xOffset, yOffset);
}

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset) {
    auto *camera = reinterpret_cast<Camera *> (glfwGetWindowUserPointer(window));
    camera->processMouseScroll(yoffset);
}