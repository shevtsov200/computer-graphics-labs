#version 330 core
out vec4 FragColor;

in vec3 vertexPositionWorld;
in vec3 fragNormal;


uniform vec3 objectColor;
uniform vec3 lightColor;

uniform vec3 lightPositionWorld;
uniform vec3 cameraPositionWorld;

void main()
{
    float ambientStrength = 0.2;
    vec3 ambientComponent = ambientStrength * lightColor;

    vec3 normal = normalize(fragNormal);
    vec3 lightDirection = normalize(lightPositionWorld - vertexPositionWorld);

    float diffuse = max(dot(normal, lightDirection), 0.0);
    vec3 diffuseComponent = diffuse * lightColor;

    float specularStrength = 0.2;
    vec3 viewDirection = normalize(cameraPositionWorld - vertexPositionWorld);
    vec3 reflectionDirection = reflect(-lightDirection, normal);
    float spec = pow(max(dot(viewDirection, reflectionDirection), 0.0), 32);
    vec3 specularComponent = specularStrength * spec * lightColor;

    vec3 result = (ambientComponent + diffuseComponent + specularComponent) * objectColor;
    FragColor = vec4(result, 1.0);
}