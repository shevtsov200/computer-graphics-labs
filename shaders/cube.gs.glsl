#version 330 core
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

out vec3 vertexPositionWorld;
out vec3 fragNormal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

vec3 calculateNormal() {
    vec3 a = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
    vec3 b = gl_in[1].gl_Position.xyz - gl_in[2].gl_Position.xyz;

    return cross(a, b);
}

void main() {
    mat4 projectionViewModel = projection * view * model;

    vec3 normal = calculateNormal();

    normal = transpose(inverse(mat3(model))) * normal;

    for(int i = 0; i < gl_in.length(); ++i) {
        gl_Position = projectionViewModel * gl_in[i].gl_Position;
        vertexPositionWorld = vec3(model * gl_in[i].gl_Position);

        fragNormal = normal;

        EmitVertex();
    }

    EndPrimitive();



}
