#version 330 core
layout(triangles) in;
layout(line_strip, max_vertices = 14) out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 lightPositionWorld;
uniform vec3 cameraPositionWorld;
uniform vec3 shapeCenterPosition;

uniform vec3 edgeColor;
uniform vec3 lightColor;
uniform vec3 normalColor;

out vec3 fragColor;

vec3 calculateNormal() {
    vec3 a = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
    vec3 b = gl_in[1].gl_Position.xyz - gl_in[2].gl_Position.xyz;

    return cross(a, b);
}

vec3 getTriangleCenter(vec4 a, vec4 b, vec4 c) {
    return vec3(
        (gl_in[0].gl_Position.x + gl_in[1].gl_Position.x + gl_in[2].gl_Position.x)/3,
        (gl_in[0].gl_Position.y + gl_in[1].gl_Position.y + gl_in[2].gl_Position.y)/3,
        (gl_in[0].gl_Position.z + gl_in[1].gl_Position.z + gl_in[2].gl_Position.z)/3
    );
}

void emitEdges() {
    for(int i = 0; i < gl_in.length(); ++i) {
        gl_Position = projection * view * model * gl_in[i].gl_Position;
        fragColor = edgeColor;
        EmitVertex();
    }

    gl_Position = projection * view * model * gl_in[0].gl_Position;
    fragColor = edgeColor;
    EmitVertex();

    EndPrimitive();
}

void emitNormal(vec3 start) {
    vec3 normal = calculateNormal();

    float vectorLength = 0.5;

    gl_Position = projection * view * model * vec4(start, 1.0f);
    fragColor = normalColor;
    EmitVertex();

    gl_Position = projection * view * model * vec4(start + normal*vectorLength, 1.0f);
    fragColor = normalColor;
    EmitVertex();

    EndPrimitive();
}

void emitLightVector(vec3 start) {
    gl_Position = projection * view * vec4(start, 1.0f);
    fragColor = lightColor;
    EmitVertex();

    vec3 lightDirection = normalize(lightPositionWorld - start);
    float lightVectorLength = length(lightPositionWorld - start);

    gl_Position = projection * view * vec4(start + lightDirection*lightVectorLength, 1.0f);
    fragColor = lightColor;
    EmitVertex();

    EndPrimitive();
}

void emitCenterAxis() {
    float length = 2.0f;
    vec3 centerColor = vec3(1.0f, 0.0f, 0.0f);
    gl_Position = projection * view * model * vec4(shapeCenterPosition, 1.0f);
    fragColor = centerColor;
    EmitVertex();

    vec3 normal = calculateNormal();
    gl_Position = projection * view * model * vec4(shapeCenterPosition + normal * length, 1.0f);
    fragColor = centerColor;
    EmitVertex();

}

void main() {
    vec3 start = getTriangleCenter(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_in[2].gl_Position);

    emitEdges();

    emitNormal(start);

    for(int i = 0; i < gl_in.length(); ++i) {
        vec3 vertexPositionWorld = vec3(model * gl_in[i].gl_Position);
        emitLightVector(vertexPositionWorld);
    }

    emitCenterAxis();
}