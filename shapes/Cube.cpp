//
// Created by root on 16.11.18.
//

#include "Cube.h"

Cube::Cube(float size) : HyperRectangle(glm::vec3(size)) {
    vertices_ = calculateVertices();

    boundBox_.initializeBounds(vertices_);

    indices_ = {
            // back
            0, 1, 3,
            1, 2, 3,

            // top
            1, 5, 6,
            1, 2, 6,

            // right
            2, 3, 7,
            3, 6, 7,

            // left
            0, 4, 5,
            0, 1, 5,

            // front
            4, 6, 7,
            4, 5, 6,

            // bottom
            0, 3, 4,
            3, 4, 7,
    };
}
