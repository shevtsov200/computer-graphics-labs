//
// Created by root on 16.11.18.
//

#ifndef OPENGL_COURSE_PROJECT_CUBE_H
#define OPENGL_COURSE_PROJECT_CUBE_H

#include <vector>
#include <glm/vec3.hpp>
#include "Shape.h"
#include "collisions/BoundBox.h"
#include "HyperRectangle.h"

class Cube : public HyperRectangle {
public:
    explicit Cube(float size);
};


#endif //OPENGL_COURSE_PROJECT_CUBE_H
