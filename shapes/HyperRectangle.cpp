//
// Created by root on 13.12.18.
//

#include "HyperRectangle.h"

HyperRectangle::HyperRectangle(glm::vec3 size) : Shape(size) {
    vertices_ = calculateVertices();

    boundBox_.initializeBounds(vertices_);

    indices_ = {
            // bottom
            0, 2, 1,
            1, 2, 3,
            // top
            6, 4, 5,
            6, 5, 7,
            // left
            0, 4, 2,
            2, 4, 6,
            // right
            5, 1, 3,
            5, 3, 7,
            // back
            4, 0, 1,
            4, 1, 5,
            // front
            2, 6, 3,
            3, 6, 7,
    };
}

const std::vector<glm::vec3> &HyperRectangle::getVertices() const {
    return vertices_;
}

const std::vector<unsigned int> &HyperRectangle::getIndices() const {
    return indices_;
}

const BoundBox &HyperRectangle::getBoundBox() const {
    return boundBox_;
}

std::vector<glm::vec3> HyperRectangle::calculateVertices() {
    return {
            glm::vec3(-size_.x, -size_.y, -size_.z) / 2.0f, //0 back bottom left
            glm::vec3(size_.x, -size_.y, -size_.z) / 2.0f, //1 back bottom right

            glm::vec3(-size_.x, -size_.y, size_.z) / 2.0f, //2 front bottom left
            glm::vec3(size_.x, -size_.y, size_.z) / 2.0f, //3 front bottom right

            glm::vec3(-size_.x, size_.y, -size_.z) / 2.0f, //4 back top left
            glm::vec3(size_.x, size_.y, -size_.z) / 2.0f, //5 back top right

            glm::vec3(-size_.x, size_.y, size_.z) / 2.0f, //6 front top left
            glm::vec3(size_.x, size_.y, size_.z) / 2.0f, //7 front top right
    };
}
