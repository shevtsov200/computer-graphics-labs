//
// Created by root on 13.12.18.
//

#ifndef OPENGL_COURSE_PROJECT_PLANE_H
#define OPENGL_COURSE_PROJECT_PLANE_H


#include "Shape.h"

class HyperRectangle : public Shape {
public:
    explicit HyperRectangle(glm::vec3 size);

    const std::vector<glm::vec3> &getVertices() const override;

    const std::vector<unsigned int> &getIndices() const override;

    const BoundBox &getBoundBox() const override;

    std::vector<glm::vec3> calculateVertices() override;
};


#endif //OPENGL_COURSE_PROJECT_PLANE_H
