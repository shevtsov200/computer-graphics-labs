//
// Created by root on 06.12.18.
//

#include <algorithm>
#include "Icosahedron.h"

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/string_cast.hpp>

Icosahedron::Icosahedron(float size) : Shape(glm::vec3(size)) {

    vertices_ = calculateVertices();


    boundBox_.initializeBounds(vertices_);

    indices_ = {
            0, 4, 1,
            0, 9, 4,
            9, 5, 4,
            4, 5, 8,
            4, 8, 1,
            8, 10, 1,
            8, 3, 10,
            5, 3, 8,
            5, 2, 3,
            2, 7, 3,
            7, 10, 3,
            7, 6, 10,
            7, 11, 6,
            11, 0, 6,
            0, 1, 6,
            6, 1, 10,
            9, 0, 11,
            9, 11, 2,
            9, 2, 5,
            7, 2, 11,

    };
}

const std::vector<glm::vec3> &Icosahedron::getVertices() const {
    return vertices_;
}

const std::vector<unsigned int> &Icosahedron::getIndices() const {
    return indices_;
}

const BoundBox &Icosahedron::getBoundBox() const {
    return boundBox_;
}

void Icosahedron::recalculateBounds(const std::vector<glm::vec3> &vertices) {
    boundBox_.initializeBounds(vertices);
}

void Icosahedron::transformVertices(glm::mat4 model) {
    vertices_ = calculateVertices();

    std::for_each(vertices_.begin(), vertices_.end(), [model](glm::vec3 &vertex) {
        vertex = glm::vec3(model * glm::vec4(vertex, 1.0f));
    });

    recalculateBounds(vertices_);
}

std::vector<glm::vec3> Icosahedron::calculateVertices() {
    const float X = 0.5257311;
    const float Z = 0.8506508;
    const float N = 0.0f;

    return {
            glm::vec3(-X, N, Z),
            glm::vec3(X, N, Z),
            glm::vec3(-X, N, -Z),
            glm::vec3(X, N, -Z),
            glm::vec3(N, Z, X),
            glm::vec3(N, Z, -X),
            glm::vec3(N, -Z, X),
            glm::vec3(N, -Z, -X),
            glm::vec3(Z, X, N),
            glm::vec3(-Z, X, N),
            glm::vec3(Z, -X, N),
            glm::vec3(-Z, -X, N),
    };
}
