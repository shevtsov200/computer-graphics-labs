//
// Created by root on 06.12.18.
//

#ifndef OPENGL_COURSE_PROJECT_ICOSAHEDRON_H
#define OPENGL_COURSE_PROJECT_ICOSAHEDRON_H

#include <vector>

#include <glm/vec3.hpp>
#include "Shape.h"

class Icosahedron : public Shape {
public:
    explicit Icosahedron(float size);

    const std::vector<glm::vec3> &getVertices() const override;

    const std::vector<unsigned int> &getIndices() const override;

    const BoundBox &getBoundBox() const override;

    std::vector<glm::vec3> calculateVertices() override;

    void recalculateBounds(const std::vector<glm::vec3> &vertices);

    void transformVertices(glm::mat4 model);
};


#endif //OPENGL_COURSE_PROJECT_ICOSAHEDRON_H
