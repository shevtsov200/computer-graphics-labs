//
// Created by root on 12.12.18.
//

#include <glm/ext/matrix_transform.hpp>
#include "Shape.h"


const glm::vec3 &Shape::getSize() const {
    return size_;
}

void Shape::setPositionWorld(glm::vec3 positionWorld) {
    positionWorld_ = positionWorld;
}

const glm::vec3 &Shape::getPositionWorld() const {
    return positionWorld_;
}

const glm::vec3 &Shape::getRotationAngles() const {
    return rotationAngles_;
}

void Shape::setRotationAngles(const glm::vec3 &rotationAngles) {
    rotationAngles_ = rotationAngles;
}

void Shape::transform() {
    model_ = glm::translate(glm::mat4(1.0f), positionWorld_);
    model_ = glm::rotate(model_, glm::radians(rotationAngles_.x), glm::vec3(1.0f, 0.0f, 0.0f));
    model_ = glm::rotate(model_, glm::radians(rotationAngles_.y), glm::vec3(0.0f, 1.0f, 0.0f));
    model_ = glm::rotate(model_, glm::radians(rotationAngles_.z), glm::vec3(0.0f, 0.0f, 1.0f));
}

const glm::mat4 &Shape::getModel() const {
    return model_;
}
