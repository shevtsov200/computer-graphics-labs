//
// Created by root on 12.12.18.
//

#ifndef OPENGL_COURSE_PROJECT_SHAPE_H
#define OPENGL_COURSE_PROJECT_SHAPE_H


#include <glm/vec3.hpp>
#include <vector>
#include "collisions/BoundBox.h"

class Shape {
public:
    explicit Shape(glm::vec3 size)
            : size_(size) {};

    virtual const std::vector<glm::vec3> &getVertices() const = 0;

    virtual const std::vector<unsigned int> &getIndices() const = 0;

    virtual const BoundBox &getBoundBox() const = 0;

    virtual std::vector<glm::vec3> calculateVertices() = 0;

    const glm::vec3 &getSize() const;

    void setPositionWorld(glm::vec3 positionWorld);

    const glm::vec3 &getPositionWorld() const;

    const glm::vec3 &getRotationAngles() const;

    void setRotationAngles(const glm::vec3 &rotationAngles);

    virtual ~Shape() = default;

    const glm::mat4 &getModel() const;

    void transform();

protected:
    BoundBox boundBox_;
    std::vector<glm::vec3> vertices_;
    std::vector<unsigned int> indices_;

    glm::vec3 size_;

    glm::vec3 positionWorld_;
    glm::vec3 rotationAngles_;

    glm::mat4 model_;
};


#endif //OPENGL_COURSE_PROJECT_SHAPE_H
