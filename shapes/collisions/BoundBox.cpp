//
// Created by root on 15.12.18.
//

#include <algorithm>

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/string_cast.hpp>
#include "BoundBox.h"

const std::vector<glm::vec3> &BoundBox::getVertices() const {
    return vertices_;
}

void BoundBox::initializeBounds(std::vector<glm::vec3> shapeVertices) {
    leftBottomBack_.x = std::min_element(shapeVertices.cbegin(), shapeVertices.cend(),
                                         [](glm::vec3 a, glm::vec3 b) { return a.x < b.x; })->x;
    rightTopFront_.x = std::max_element(shapeVertices.cbegin(), shapeVertices.cend(),
                                        [](glm::vec3 a, glm::vec3 b) { return a.x < b.x; })->x;

    leftBottomBack_.y = std::min_element(shapeVertices.cbegin(), shapeVertices.cend(),
                                         [](glm::vec3 a, glm::vec3 b) { return a.y < b.y; })->y;
    rightTopFront_.y = std::max_element(shapeVertices.cbegin(), shapeVertices.cend(),
                                        [](glm::vec3 a, glm::vec3 b) { return a.y < b.y; })->y;

    leftBottomBack_.z = std::min_element(shapeVertices.cbegin(), shapeVertices.cend(),
                                         [](glm::vec3 a, glm::vec3 b) { return a.z < b.z; })->z;
    rightTopFront_.z = std::max_element(shapeVertices.cbegin(), shapeVertices.cend(),
                                        [](glm::vec3 a, glm::vec3 b) { return a.z < b.z; })->z;

    size_ = rightTopFront_ - leftBottomBack_;

    center_ = leftBottomBack_ + (size_ / 2.0f);

    vertices_ = {
            leftBottomBack_, //0 back bottom left
            glm::vec3(rightTopFront_.x, leftBottomBack_.y, leftBottomBack_.z), //1 back bottom right

            glm::vec3(leftBottomBack_.x, leftBottomBack_.y, rightTopFront_.z), //2 front bottom left
            glm::vec3(rightTopFront_.x, leftBottomBack_.y, rightTopFront_.z), //3 front bottom right

            glm::vec3(leftBottomBack_.x, rightTopFront_.y, leftBottomBack_.z), //4 back top left
            glm::vec3(rightTopFront_.x, rightTopFront_.y, leftBottomBack_.z), //5 back top right

            glm::vec3(leftBottomBack_.x, rightTopFront_.y, rightTopFront_.z), //6 front top left
            rightTopFront_, //7 front top right
    };
}

bool BoundBox::intersects(const BoundBox &anotherBox) const {
    glm::vec3 centerDistance = glm::abs(center_ - anotherBox.center_);
    glm::vec3 sizeSum = (size_ + anotherBox.size_) / 2.0f;

    return glm::all(glm::lessThan(centerDistance, sizeSum));
}

glm::vec3 BoundBox::getOverlap(const BoundBox &anotherBox) const {
    glm::vec3 centerDistance = glm::abs(center_ - anotherBox.center_);
    glm::vec3 sizeSum = (size_ + anotherBox.size_) / 2.0f;

    glm::vec3 overlap = glm::vec3(0.0f);

    if (glm::lessThan(centerDistance, sizeSum).y) {
        overlap.y = glm::abs(leftBottomBack_.y - anotherBox.getRightTopFront().y);
    }

    return overlap;
}

const glm::vec3 &BoundBox::getCenter() const {
    return center_;
}

const BoundBox
BoundBox::getBoundBoxWorldCoordinates(const glm::mat4 &model, std::vector<glm::vec3> shapeVertices) const {
    std::vector<glm::vec3> shapeVerticesWorldCoordinates;

    std::transform(shapeVertices.cbegin(), shapeVertices.cend(),
                   std::back_inserter(shapeVerticesWorldCoordinates), [model](const glm::vec3 vertex) {
                return glm::vec3(model * glm::vec4(vertex, 1.0f));
            });

    BoundBox boundBoxWorldCoordinates;
    boundBoxWorldCoordinates.initializeBounds(shapeVerticesWorldCoordinates);

    return boundBoxWorldCoordinates;
}

const glm::vec3 &BoundBox::getRightTopFront() const {
    return rightTopFront_;
}
