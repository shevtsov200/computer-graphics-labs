//
// Created by root on 15.12.18.
//

#ifndef OPENGL_COURSE_PROJECT_BOUNDBOX_H
#define OPENGL_COURSE_PROJECT_BOUNDBOX_H


#include <glm/glm.hpp>
#include <vector>

class BoundBox {
public:
    BoundBox() = default;

    void initializeBounds(std::vector<glm::vec3> shapeVertices);

    const std::vector<glm::vec3> &getVertices() const;

    bool intersects(const BoundBox &anotherBox) const;

    glm::vec3 getOverlap(const BoundBox &anotherBox) const;

    const glm::vec3 &getCenter() const;

    const glm::vec3 &getRightTopFront() const;

    const BoundBox getBoundBoxWorldCoordinates(const glm::mat4 &model, std::vector<glm::vec3> shapeVertices) const;

private:
    glm::vec3 leftBottomBack_;
    glm::vec3 rightTopFront_;

    glm::vec3 size_;

    glm::vec3 center_;

    std::vector<glm::vec3> vertices_;
};


#endif //OPENGL_COURSE_PROJECT_BOUNDBOX_H
