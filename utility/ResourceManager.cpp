//
// Created by root on 20.11.18.
//

#include <fstream>
#include "ResourceManager.h"

std::map<std::string, Shader> ResourceManager::shaders_;


Shader ResourceManager::loadShader(const GLchar *vertexShaderFile, const GLchar *fragmentShaderFile,
                                   const GLchar *geometryShaderFile, const GLchar *shaderName) {
    shaders_[shaderName] = loadShaderFromFile(vertexShaderFile, fragmentShaderFile, geometryShaderFile);

    return shaders_[shaderName];
}

Shader &ResourceManager::getShader(std::string name) {
    return shaders_[name];
}

void ResourceManager::clear() {
    for (auto shaderIterator : shaders_) {
        glDeleteProgram(shaderIterator.second.mShaderProgramId);
    }
}

Shader ResourceManager::loadShaderFromFile(const GLchar *vertexName,
                                           const GLchar *fragmentName,
                                           const GLchar *geometryName) {

    Shader shader("../shaders/", vertexName, fragmentName, geometryName);
    return shader;

}
//
//std::string Shader::readShaderFile(const std::string &shaderPath) const {
//    try {
//        std::ifstream shaderFileStream;
//
//        shaderFileStream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
//        shaderFileStream.open(shaderPath);
//
//        std::stringstream vertexShaderStream;
//        vertexShaderStream << shaderFileStream.rdbuf();
//
//        shaderFileStream.close();
//
//        std::string shaderSourceCode;
//        shaderSourceCode = vertexShaderStream.str();
//
//        return shaderSourceCode;
//    } catch (const std::ifstream::failure &e) {
//        std::cerr << "error when reading shader file" << std::endl;
//    }
//}
