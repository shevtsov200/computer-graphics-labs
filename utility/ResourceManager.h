//
// Created by root on 20.11.18.
//

#ifndef OPENGL_COURSE_PROJECT_RESOURCEMANAGER_H
#define OPENGL_COURSE_PROJECT_RESOURCEMANAGER_H

#include "Shader.h"

#include <glad/glad.h>
#include <string>
#include <map>


class ResourceManager {
public:
    static Shader loadShader(const GLchar *vertexShaderFile,
                             const GLchar *fragmentShaderFile,
                             const GLchar *geometryShaderFile,
                             const GLchar *shaderName);

    static Shader &getShader(std::string name);

    static void clear();

private:
    static std::map<std::string, Shader> shaders_;

    ResourceManager() = default;

    static Shader loadShaderFromFile(const GLchar *vertexName,
                                     const GLchar *fragmentName,
                                     const GLchar *geometryName = nullptr);
};


#endif //OPENGL_COURSE_PROJECT_RESOURCEMANAGER_H
