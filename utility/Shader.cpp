//
// Created by root on 10.11.18.
//

#include "Shader.h"
#include <fstream>
#include <sstream>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>


//TODO: rewrite handling errors
Shader::Shader(const GLchar *shaderPath,
               const GLchar *vertexName,
               const GLchar *fragmentName,
               const GLchar *geometryName) {
    unsigned int vertex = createShader(shaderPath, vertexName, GL_VERTEX_SHADER);
    unsigned int fragment = createShader(shaderPath, fragmentName, GL_FRAGMENT_SHADER);

    std::vector<unsigned int> shaders = {vertex, fragment};

    if (geometryName != nullptr) {
        unsigned int geometry = createShader(shaderPath, geometryName, GL_GEOMETRY_SHADER);
        shaders.push_back(geometry);
    }

    mShaderProgramId = createShaderProgram(shaders);
}

void Shader::setBoolUniform(const std::string &name, bool value) const {
    int location = glGetUniformLocation(mShaderProgramId, name.c_str());
    glUniform1i(location, (int) value);
}

void Shader::setIntUniform(const std::string &name, int value) const {
    int location = glGetUniformLocation(mShaderProgramId, name.c_str());
    glUniform1i(location, value);
}

void Shader::setFloatUniform(const std::string &name, float value) const {
    int location = glGetUniformLocation(mShaderProgramId, name.c_str());
    glUniform1f(location, value);
}

void Shader::setVec3Uniform(const std::string &name, const glm::vec3 &value) {
    int location = glGetUniformLocation(mShaderProgramId, name.c_str());
    glUniform3fv(location, 1, &value[0]);
}

void Shader::setVec3Uniform(const std::string &name, float x, float y, float z) {
    int location = glGetUniformLocation(mShaderProgramId, name.c_str());
    glUniform3f(location, x, y, z);
}

void Shader::setMat4Uniform(const std::string &name, glm::mat4 value) {
    int location = glGetUniformLocation(mShaderProgramId, name.c_str());
    const int MATRICES_NUMBER = 1;
    glUniformMatrix4fv(location, MATRICES_NUMBER, GL_FALSE, glm::value_ptr(value));
}

void Shader::use() {
    glUseProgram(mShaderProgramId);
}

unsigned int Shader::createShaderProgram(const std::vector<unsigned int> &shaders) const {
    unsigned int shaderProgramId = glCreateProgram();

    for (unsigned int shader : shaders) {
        glAttachShader(shaderProgramId, shader);
    }

    glLinkProgram(shaderProgramId);

    int success;
    glGetProgramiv(shaderProgramId, GL_LINK_STATUS, &success);

    if (!success) {
        char infoLog[INFO_LOG_SIZE];

        glGetProgramInfoLog(shaderProgramId, INFO_LOG_SIZE, nullptr, infoLog);
        std::cerr << "shader program linking error:\n" << infoLog << std::endl;
    }

    for (unsigned int shader : shaders) {
        glDeleteShader(shader);
    }

    return shaderProgramId;
}

std::string Shader::readShaderFile(const std::string &shaderPath) const {
    try {
        std::ifstream shaderFileStream;

        shaderFileStream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        shaderFileStream.open(shaderPath);

        std::stringstream vertexShaderStream;
        vertexShaderStream << shaderFileStream.rdbuf();

        shaderFileStream.close();

        std::string shaderSourceCode;
        shaderSourceCode = vertexShaderStream.str();

        return shaderSourceCode;
    } catch (const std::ifstream::failure &e) {
        std::cerr << "error when reading shader file" << std::endl;
    }
}

unsigned int Shader::compileShader(const std::string shaderSourceCodeString, const GLenum shaderType) const {
    unsigned int shaderId = glCreateShader(shaderType);
    const char *shaderSourceCode = shaderSourceCodeString.c_str();
    glShaderSource(shaderId, 1, &shaderSourceCode, nullptr);
    glCompileShader(shaderId);

    int success;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
    if (!success) {
        char infoLog[INFO_LOG_SIZE];

        glGetShaderInfoLog(shaderId, INFO_LOG_SIZE, nullptr, infoLog);

        std::stringstream errorMessage;
        errorMessage << "shader compilation error: " << infoLog << std::endl;
        throw std::invalid_argument(errorMessage.str());
    }

    return shaderId;
}

std::string Shader::createShaderPathString(const std::string &shaderDirectoryPath,
                                           const std::string &shaderName,
                                           const std::string &shaderTypeFormat) const {
    const std::string GLSL_FORMAT = "glsl";

    const std::string shaderPathString = shaderDirectoryPath + shaderName
                                         + "." + shaderTypeFormat + "." + GLSL_FORMAT;
    std::cout << shaderPathString << std::endl;

    return shaderPathString;
}

unsigned int Shader::createShader(const std::string &shadersDirectoryPath,
                                  const std::string &shaderName, GLenum shaderType) const {
    std::string fragmentFormat = getShaderFormat(shaderType);

    const std::string fragmentPathString = createShaderPathString(shadersDirectoryPath,
                                                                  shaderName, fragmentFormat);
    const GLchar *fragmentPath = fragmentPathString.c_str();

    std::string fragmentCode = readShaderFile(fragmentPath);
    unsigned int fragment = compileShader(fragmentCode, shaderType);

    return fragment;
}

std::string Shader::getShaderFormat(GLenum type) const {
    const std::string VERTEX_FORMAT = "vs";
    const std::string GEOMETRY_FORMAT = "gs";
    const std::string FRAGMENT_FORMAT = "fs";

    std::string shaderFormat;

    switch (type) {
        case GL_VERTEX_SHADER:
            shaderFormat = VERTEX_FORMAT;
            break;
        case GL_GEOMETRY_SHADER:
            shaderFormat = GEOMETRY_FORMAT;
            break;
        case GL_FRAGMENT_SHADER:
            shaderFormat = FRAGMENT_FORMAT;
            break;
        default:
            std::stringstream errorMessage;
            errorMessage << "unknown shader type" << std::endl;
            throw std::invalid_argument(errorMessage.str());
            break;
    }

    return shaderFormat;
}
