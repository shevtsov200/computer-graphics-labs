#ifndef SHADER_H
#define SHADER_H

#include <glad/glad.h>
#include <string>
#include <glm/detail/type_mat4x4.hpp>
#include <vector>

class Shader {
public:
    unsigned int mShaderProgramId;

    Shader() = default;

    Shader(const GLchar *shaderPath,
           const GLchar *vertexName,
           const GLchar *fragmentName,
           const GLchar *geometryName = nullptr);

    void use();

    void setBoolUniform(const std::string &name, bool value) const;

    void setIntUniform(const std::string &name, int value) const;

    void setFloatUniform(const std::string &name, float value) const;

    void setMat4Uniform(const std::string &name, glm::mat4 value);

    void setVec3Uniform(const std::string &name, const glm::vec3 &value);

    void setVec3Uniform(const std::string &name, float x, float y, float z);

private:
    static const int INFO_LOG_SIZE = 1024;

    unsigned int compileShader(std::string shaderSourceCodeString, GLenum shaderType) const;

    std::string readShaderFile(const std::string &shaderPath) const;

    unsigned int createShaderProgram(const std::vector<unsigned int> &shaders) const;

    std::string createShaderPathString(const std::string &shaderDirectoryPath,
                                       const std::string &shaderName,
                                       const std::string &shaderTypeFormat) const;

    unsigned int createShader(const std::string &shadersDirectoryPath,
                              const std::string &shaderName, GLenum shaderType) const;

    std::string getShaderFormat(GLenum type) const;
};

#endif