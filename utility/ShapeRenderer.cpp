//
// Created by root on 12.12.18.
//

#include <glm/ext/matrix_transform.hpp>
#include "ShapeRenderer.h"

ShapeRenderer::ShapeRenderer(Shape &shape) {
    initRenderData(shape);
}

void ShapeRenderer::drawShape(const Shape &shape, Shader &shader, glm::mat4 projection, glm::mat4 view) {
    shader.use();

    shader.setMat4Uniform("projection", projection);
    shader.setMat4Uniform("view", view);
    shader.setMat4Uniform("model", shape.getModel());

    glBindVertexArray(vao_);
    glDrawElements(GL_TRIANGLES, shape.getIndices().size(), GL_UNSIGNED_INT, nullptr);
}

void ShapeRenderer::initRenderData(Shape &shape) {
    const size_t NUMBER_OF_OBJECTS = 1;

    glGenVertexArrays(NUMBER_OF_OBJECTS, &vao_);
    glGenBuffers(NUMBER_OF_OBJECTS, &vbo_);
    glGenBuffers(NUMBER_OF_OBJECTS, &ebo_);

    glBindVertexArray(vao_);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glBufferData(GL_ARRAY_BUFFER, shape.getVertices().size() * sizeof(glm::vec3), shape.getVertices().data(),
                 GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, shape.getIndices().size() * sizeof(unsigned int), shape.getIndices().data(),
                 GL_STATIC_DRAW);


    const int COORDINATES_INDEX = 0;
    const int COORDINATES_COMPONENTS_NUMBER = 3;
    const size_t COORDINATES_OFFSET = 0;
    const size_t COORDINATES_BYTE_SIZE = COORDINATES_COMPONENTS_NUMBER * sizeof(float);

    const size_t STRIDE = COORDINATES_BYTE_SIZE;

    glVertexAttribPointer(COORDINATES_INDEX, COORDINATES_COMPONENTS_NUMBER, GL_FLOAT, GL_FALSE,
                          STRIDE, (void *) COORDINATES_OFFSET);
    glEnableVertexAttribArray(COORDINATES_INDEX);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
}

void ShapeRenderer::clear() {
    glDeleteVertexArrays(1, &vao_);
    glDeleteBuffers(1, &vbo_);
    glDeleteBuffers(1, &ebo_);
}

const GLuint &ShapeRenderer::getVbo() const {
    return vbo_;
}
