//
// Created by root on 12.12.18.
//

#ifndef OPENGL_COURSE_PROJECT_SHAPERENDERER_H
#define OPENGL_COURSE_PROJECT_SHAPERENDERER_H


#include "Shader.h"
#include "../shapes/Shape.h"

class ShapeRenderer {
public:
    explicit ShapeRenderer(Shape &shape);

    ShapeRenderer() = default;
    ~ShapeRenderer() = default;

    void drawShape(const Shape &shape, Shader &shader, glm::mat4 projection, glm::mat4 view);

    void clear();

    void initRenderData(Shape &shape);

    const GLuint &getVbo() const;

private:
    GLuint vao_;
    GLuint vbo_;
    GLuint ebo_;


};


#endif //OPENGL_COURSE_PROJECT_SHAPERENDERER_H
